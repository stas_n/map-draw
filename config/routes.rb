Rails.application.routes.draw do
  root 'fields#index'
  resources :fields
  post 'save_shape' => 'fields#save_shape'
end

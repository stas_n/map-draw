source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

ruby '2.4.1'
gem 'activerecord-postgis-adapter'
gem 'bootstrap-sass', '~> 3.3.6'
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'leaflet-draw-rails'
gem 'leaflet-rails'
gem 'pg'
gem 'puma', '~> 3.7'
gem 'rails', '~> 5.1.0'
gem 'rgeo-activerecord'
gem 'rgeo-geojson'
gem 'sass-rails', '~> 5.0'
gem 'slim'
gem 'turbolinks', '~> 5'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'uglifier', '>= 1.3.0'

group :development, :test do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'capybara'
  gem 'chromedriver-helper'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
  gem 'faker', git: 'https://github.com/stympy/faker.git'
  gem 'listen'
  gem 'pry'
  gem 'pry-rails'
  gem 'rails-controller-testing'
  gem 'rspec-core'
  gem 'rspec-json_expectations'
  gem 'rspec-rails', '~> 3.5'
  gem 'rspec-support'
  gem 'rubocop', require: false
  gem 'selenium-webdriver'
  gem 'spork'
end

group :development do
  gem 'spring'
  gem 'web-console', '~> 2.0'
end

class CreateFields < ActiveRecord::Migration
  def change
    execute "CREATE EXTENSION IF NOT EXISTS postgis;"

    create_table :fields do |t|
      t.string :name
      t.column :shape, :multi_polygon, geographic: true, srid: 4326
      t.timestamps null: false
    end
  end
end
class Field < ActiveRecord::Base
  default_scope { order(created_at: :desc) }

  class << self

    def to_map_coords(shape)
      RGeo::GeoJSON.encode(shape).to_json
    end

    def to_multipolygon(polygon)
      sql = "SELECT ST_AsText(ST_Multi(ST_GeomFromText('#{polygon}')));"
      pg_result(sql)
    end

    def area(shape)
      sql = "SELECT ST_Perimeter(ST_GeomFromText('#{shape}'));"
      pg_result(sql)
    end

    private

    def pg_result(sql)
      result = ActiveRecord::Base.connection.execute(sql)
      result.getvalue(0, 0)
    end
  end
end

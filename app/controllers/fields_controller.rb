class FieldsController < ApplicationController
  before_action :set_field, only: %i[show edit update destroy save_shape]

  def index
    @fields = Field.all
  end

  def show; end

  def new
    @field = Field.new
  end

  def edit; end

  def create
    params['field']['shape'] = decoded_shape(params['field']['shape'])
    @field = Field.new(field_params)
    respond_to do |format|
      if @field.save
        format.html { redirect_to @field, notice: 'Field was successfully created.' }
        format.json { render :show, status: :created, location: @field }
      else
        format.html { render :new }
        format.json { render json: @field.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      shape =  params['field'].except('shape') if params['field']['shape'].include?('MULTIPOLYGON')
      params['field']['shape'] = decoded_shape(params['field']['shape']) unless shape
      if @field.update(field_params)
        format.html { redirect_to @field, notice: 'Field was successfully updated.' }
        format.json { render :show, status: :ok, location: @field }
      else
        format.html { render :edit }
        format.json { render json: @field.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @field.destroy
    respond_to do |format|
      format.html { redirect_to fields_url, notice: 'Field was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def decoded_shape(params)
    coords = RGeo::GeoJSON.decode(params, json_parser: :json)
    Field.to_multipolygon(coords.geometry.as_text)
  end

  private

  def set_field
    @field = Field.find(params[:id])
  end

  def field_params
    params.require(:field).permit(:name, :shape)
  end
end

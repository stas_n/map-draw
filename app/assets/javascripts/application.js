// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require rails-ujs
//= require turbolinks
//= require leaflet
//= require leaflet.draw
//= require_tree .


function init(need_to_draw) {
    $(document).ready(function () {

        var mymap = L.map('map').setView([50.4546600, 30.5238000], 6);

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            maxZoom: 18,
            attribution: 'test'
        }).addTo(mymap);
        if (need_to_draw === true) {
            draw(mymap);
        }
        shape_on_map(mymap);
    });


}

function shape_on_map(mymap) {
    var shape = $('#map').data('shape');
    var polygon = L.geoJSON(shape).addTo(mymap);
    if (shape) {
        mymap.fitBounds(polygon.getBounds());

    }
}


function draw(mymap) {

    // mymap.on('click', function(e) {
    //     alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng)
    // });

    var drawnItems = new L.FeatureGroup();
    mymap.addLayer(drawnItems);
    var drawControl = new L.Control.Draw({

        draw: {
            polyline: false,
            circle: false,
            // rectangle: false,
            marker: false,
            shapeOptions: {
                color: '#bada55'
            }
        },
        edit: {
            featureGroup: drawnItems
        }
    });


    mymap.addControl(drawControl);


    mymap.on('draw:created', function (e) {
    });
    mymap.on('draw:edited', function (e) {
    });
    mymap.on('draw:deleted', function () {
    });
    mymap.on('draw:created', function (e) {
            layer = e.layer;
            var shape = layer.toGeoJSON();
            drawnItems.addLayer(layer);
            $('#hidden-shape').val(JSON.stringify(shape))
        }
    );

}


var getShapes = function (drawnItems) {

    var shapes = [];

    drawnItems.eachLayer(function (layer) {
        // Note: Rectangle extends Polygon. Polygon extends Polyline.
        // Therefore, all of them are instances of Polyline
        if (layer instanceof L.Polyline) {
            shapes.push(layer.getLatLngs())
        }

        if (layer instanceof L.Circle) {
            shapes.push([layer.getLatLng()])
        }

        if (layer instanceof L.Marker) {
            shapes.push([layer.getLatLng()]);
        }

    });

    return shapes;
}

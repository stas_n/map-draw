require 'rubygems'
require 'spork'

Spork.prefork do
  ENV['RAILS_ENV'] ||= 'test'

  require File.expand_path('../../config/environment', __FILE__)

  require 'capybara/rspec'
  require 'rspec/rails'
  require 'database_cleaner'
  require 'factory_girl'
  require 'rspec/json_expectations'

  Dir[Rails.root + 'spec/support/**/*.rb'].map(&method(:require))
  RSpec.configure do |config|
    config.include Capybara::DSL
    config.include FactoryGirl::Syntax::Methods
    config.mock_with :rspec
    config.order = :random
    config.use_transactional_fixtures = false
    config.infer_spec_type_from_file_location!
    config.raise_errors_for_deprecations!

    config.before(:suite) do
      DatabaseCleaner.strategy = :transaction
      DatabaseCleaner.clean_with(:truncation, { except: %w[spatial_ref_sys] })
    end

    config.before do
      DatabaseCleaner.start
      ActionMailer::Base.deliveries.clear
    end

    config.after do
      DatabaseCleaner.clean
    end
  end
end

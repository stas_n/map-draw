require 'spec_helper'

describe Field do

  let!(:field) { create(:field, created_at: Date.today) }
  let!(:field1) { create(:field, created_at: Date.today - 1.day) }

  it 'should have default scope' do
    expect(described_class.all.to_sql).to eq described_class.all.order(created_at: :desc).to_sql
  end

  it 'should return json from shape' do
    expect(described_class.to_map_coords(field1.shape)).to include_json(
                                                               { type: 'MultiPolygon',
                                                                 coordinates: [[[[52.03125, 65.8027763934024], [58.0078125, 54.3677585240684],
                                                                                 [129.0234375, 57.7041472343419], [154.3359375, 65.8027763934024],
                                                                                 [141.6796875, 74.4021625984244], [51.6796875, 71.074056463361],
                                                                                 [52.03125, 65.8027763934024]]]] }
                                                           )
  end


  it 'should convert to multipolygon' do
    polygon = { "type": 'Polygon',
                "coordinates": [[[26.136474609375004, 50.5064398321055],
                                 [26.444091796875004, 50.51342652633956],
                                 [26.444091796875004, 50.72254683363231],
                                 [25.993652343750004, 50.71559113343383],
                                 [26.136474609375004, 50.5064398321055]]]
    }.as_json
    coords = RGeo::GeoJSON.decode(polygon, json_parser: :json)
    expect(described_class.to_multipolygon(coords)).to include('MULTIPOLYGON')
  end


  it 'calcualate area of shape' do
    expect(described_class.area(field.shape)).to eq(221.218943887022)
  end
end

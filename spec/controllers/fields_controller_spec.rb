require 'spec_helper'

RSpec.describe FieldsController, type: :controller do

  let(:field) { create(:field) }

  it 'renders the index template' do
    get :index
    expect(response.status).to eq(200)
    expect(response).to render_template :index
  end

  it 'should get new' do
    get :new
    expect(response.status).to eq(200)
    expect(response).to render_template :new
  end

  it 'should create field' do
    shape = { type: 'Feature', properties: {}, geometry: { type: 'Polygon', coordinates: [[[19.714965820312504, 50.33844888725473], [19.714965820312504, 50.61461743826628], [20.028076171875004, 50.61461743826628], [20.028076171875004, 50.33844888725473], [19.714965820312504, 50.33844888725473]]] } }.to_json
    expect {
      post :create, params: { field: { name: 's', shape: shape } }
    }.to change(Field, :count).by(1)
  end

  it 'should show field' do
    get :show, params: { id: field.id }
    expect(response.status).to eq(200)
  end


  it 'should get edit' do
    get :edit, params: { id: field.id }
    expect(response.status).to eq(200)
    expect(response).to render_template :edit
  end


  it 'should destroy field' do
    field = create(:field)
    expect{
      delete :destroy, params:{id: field.id}
    }.to change(Field,:count).by(-1)
    expect(response.status).to eq(302)
  end
end
